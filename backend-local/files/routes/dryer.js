var express = require('express');
var router = express.Router();
const db = require('../db/db.js');



router.use(express.json());

router.post('*', async function(req, res, next) {
  req.col = "";
  req.val = "";
  for (const key in req.body){
    req.col += key + ", " ;
    req.val += req.body[key] + ", ";
  }
  req.val = req.val.substring(0, req.val.length - 2);
  req.col = req.col.substring(0, req.col.length - 2);
  next();
});

router.get('/chain', async function(req, res, next) {
  let data = await db.query(`SELECT chain FROM data1 WHERE chain is NOT NULL ORDER BY id DESC LIMIT 1;`);
  res.json(data[0]);
});

router.get('/stats', async function(req, res, next) {
  let ret = {};
  let data = await db.query(`SELECT AVG(a.chain_speed) as chain_speed FROM (SELECT chain_speed from data1 WHERE chain_speed is NOT NULL ORDER BY id DESC LIMIT 2) a;`);
  ret.chain_speed = data[0].chain_speed;
  data = await db.query(`SELECT (chain_speed/50*0.6*60*
  (select value from constants where name="width_meters")*
  (SELECT AVG(a.distance) FROM (SELECT distance from data1 where distance is not null order by id desc limit 5) a)/
  1000*
  (select a.density from grainAnalysis 
    left join grainMeasurements as a on grainAnalysis.measurement_in = a.id and a.density is not null
    order by a.time desc limit 1)
  /1000) as flow 
  FROM data1 WHERE chain_speed is NOT NULL ORDER BY id DESC LIMIT 1;`);
  ret.flow = data[0].flow;

  data = await db.query(`SELECT (chain_speed/50*0.6*60*
  (select value from constants where name="width_meters")*
  (SELECT AVG(a.distance) FROM (SELECT distance from data1 where distance is not null order by id desc limit 5) a)/
  1000*
  (select a.density from grainAnalysis 
    left join grainMeasurements as a on grainAnalysis.measurement_in = a.id and a.density is not null
    order by a.time desc limit 1)*
        
   (select (1-(grain_in.moisture-modes.bazes_mitrums)*0.01) from grainAnalysis 
    left join grainMeasurements as grain_in on grainAnalysis.measurement_in = grain_in.id
    left join legacy_modes as modes on modes.id = grainAnalysis.legacy_mode
    order by grain_in.time desc limit 1)
   
  /1000) as flow_out 
  FROM data1 WHERE chain_speed is NOT NULL ORDER BY id DESC LIMIT 1;`);
  ret.flow_out = data[0].flow_out;
  res.json(ret);
});

router.get('/all', async function(req, res, next) {
  let fields = await db.query(`SHOW COLUMNS FROM data1; `);
  let q = `SELECT `
  fields.forEach(item => {
    q += `(SELECT ${item.Field} from data1 WHERE ${item.Field} is NOT null order by id desc limit 1) as ${item.Field},`;
  });
  q = q.substring(0, q.length - 1);
  q+=';';

  let data = await db.query(q);
  let dataArr = [];
  for (const key in data[0]) {
    if (Object.hasOwnProperty.call(data[0], key)) {
      const element = data[0][key];
      dataArr.push({key, value:element});
      
    }
  }
  res.json(dataArr);
});
// select
//   (select col1 from tbl where col1 is not null order by timestamp desc limit 1) as col1,
//   (select col2 from tbl where col2 is not null order by timestamp desc limit 1) as col2,
//   (select col3 from tbl where col3 is not null order by timestamp desc limit 1) as col3


router.post('/ingest', async function(req, res, next) {
  let data = await db.query(`INSERT INTO data1 (${req.col}) VALUES (${req.val});`);
  console.log(data.insertId);
  res.json(req.body);
});


module.exports = router;
