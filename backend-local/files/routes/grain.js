var express = require('express');
var router = express.Router();
const db = require('../db/db.js');
const Net = require('net');

function ConvertStringToHex(str) {
  var arr = [];
  for (var i = 0; i < str.length; i++) {
    if (str.charCodeAt(i).toString(16) ==="a" || str.charCodeAt(i).toString(16) === "d")
    {
        arr[i] = ("0" + str.charCodeAt(i).toString(16)).slice(-4);
    }else{
        arr[i] = (str.charCodeAt(i).toString(16)).slice(-4);
    }
  }
  return arr.join("");
}

router.use(express.json());


router.get("*/print/:id", async function(req, res, next){

  let data = (await db.query(`SELECT * FROM grainAnalysisView WHERE id=${req.params.id};`))[0];
  let owner = (await db.query(`SELECT name FROM owners WHERE id=${data.owner};`))[0];
  let time_in_str;
  let time_out_str;
  if(data.time_in){
    let time_in_temp = new Date(data.time_in);
    let time_in = new Date(time_in_temp.getTime() + (3 * 3600000));
    time_in_str = time_in.toISOString().slice(0,16).replace("T"," ");
  }
  if(data.time_out){
    let time_out_temp = new Date(data.time_out);
    let time_out = new Date(time_out_temp.getTime() + (3 * 3600000));
    time_out_str = time_out.toISOString().slice(0,16).replace("T"," ");
  }


  const port = 9100;
  const host = 'print.kalte.lan';
  const client = new Net.Socket();
  client.connect({ port: port, host: host }, function() {
      console.log('TCP connection established with the server.');	
      function toHex(parsable, decimals=0, size=null){
        console.log(parsable);
        let str = parsable;
        if (typeof parsable === 'number'){
          str = parsable.toFixed(decimals ? decimals : 0);
            let diff = size-str.length;
            for (let i = 0; i < diff;  i++) {
              str = " " + str;       
            }
          
        }

        return str.split("")
        .map(c => c.charCodeAt(0).toString(16).padStart(2, "0"))
        .join("");
      }
      let commands = '';
      commands += "1B696100"; //set command mode to esc/p
      commands += "1b40"; //init
      commands += "1b694c00"; //set rotation to landscape
      commands += "1b7400"; //set char code
      // commands += "1b284302000202"; //set page length
      commands += "1b240000"; //set horizontal pos
      commands += "1b285602000000"; //set veritcal pos
      commands += "1b6b09"; //set to outline font
      // commands += "1b4120"; //set line feed
      // commands += "1B4a10"; //forward paper feed     
      // commands += "1b241000"; //horizontal offset        
      commands += "1b58003200"; //set size to 12pt        
      // commands += "1b248001"; //set horizontal position  
      let col1 = "09"; 
      let col2 = "09AF09"; 
      // let col1 = "1b246001"; 
      // let col2 = "1b24E001BB1b241002"; 

      commands += "1b440B00"; //set tabs   

      // commands += toHex("123456789ABCDEF0123456789ABCDEF0") + "0D";      
      commands += "1b58006400"; //set size to 12pt   
      commands += toHex("ID") + col1 + "1b45" + toHex(req.params.id) + "1b46" + "0D";     
      commands += "1b58003200"; //set size to 12pt    
      commands += toHex("ipasnieks") + col1 + toHex(owner.name) + "0D";
      commands += toHex("kultura") + col1 + toHex(data.name) + "0D";
      commands += toHex("glabatuve") + col1 + toHex(data.storage ? data.storage : "-") + "0D";
      commands += toHex("ieeja") + col1 + toHex(time_in_str ? time_in_str : "-") + "0D";
      commands += toHex("izeja") + col1 + toHex(time_out_str ? time_out_str : "-") + "0D";


      // commands += toHex("parametrs") + col1 + toHex("pirms") + col2 + toHex("pec") + "0D";
      commands += toHex("---------------------------") + "0D";
      commands += "1b4410151700"; //set tabs   
      commands += toHex("mitrums, %") + col1 + toHex((data.moist_in ? data.moist_in : "-"), 1, 4) + col2 + toHex(data.moist_out ? data.moist_out : "-", 1,4) + "0D";
      commands += toHex("temperatura, C") + col1 + toHex(data.temp_in ? data.temp_in : "-", 1, 4) + col2 + toHex(data.temp_out ? data.temp_out : "-", 1,4) + "0D";
      commands += toHex("tilpummasa, g/l") + col1 + toHex(data.dens_in ? data.dens_in : "-", 0, 4) + col2 + toHex(data.dens_out ? data.dens_out : "-", 0, 4) + "0D";
      // commands += 
      // toHex("|") + "1b240000" +  
      // toHex("|") + "1b241000" + 
      // toHex("|") + "1b242000" + 
      // toHex("|") + "1b243000" + 
      // toHex("|") + "1b244000" +  
      // toHex("|") + "1b245000" + 
      // toHex("|") + "1b246000" + 
      // toHex("|") + "1b247000" + 
      // toHex("|") + "1b248000" + 
      // toHex("|") + "1b249000" + 
      // toHex("|") + "1b24A000" + 
      // toHex("|") + "1b24B000" + 
      // toHex("|") + "1b24C000" + 
      // toHex("|") + "1b24D000" + 
      // toHex("|") + "1b24E000" + 
      // toHex("|") + "1b24F000" + 
      // toHex("|") + "1b240001" +  
      // toHex("|") + "1b241001" + 
      // toHex("|") + "1b242001" + 
      // toHex("|") + "1b243001" + 
      // toHex("|") + "1b244001" +  
      // toHex("|") + "1b245001" + 
      // toHex("|") + "1b246001" + 
      // toHex("|") + "1b247001" + 
      // toHex("|") + "1b248001" + 
      // toHex("|") + "1b249001" + 
      // toHex("|") + "1b24A001" + 
      // toHex("|") + "1b24B001" + 
      // toHex("|") + "1b24C001" + 
      // toHex("|") + "1b24D001" + 
      // toHex("|") + "1b24E001" + 
      // toHex("|") + "1b24F001" + 
      //             "0D";
      // commands += "0A"; //line feed    
      commands += "0C"; //page feed      
                                             
      // let printInit = "1b24100034343434340A35353535";
      
      // let printerCommandString = 
      //     "m m\r\n\
      //     J\r\n\
      //     H 100\r\n\
      //     S l1;0,0,68,70,100\r\n\
      //     O R\r\n\
      //     T:Text1;6.67,13.06,0,3,4.59,k;"+ "lietas" + "\r\n\
      //     T:Text2;6.67,19.67,0,3,4.59,k;"+ "leitas2" + "\r\n\
      //     T:Text3;6.67,26.38,0,3,4.59,k;"+ "asddddd" + "\r\n";
      
      // printerCommandString +=
      //     "T:Text6;6.67,46.58,0,3,4.59,k;"+ "Amount: " + "112" + "\r\n\
      //     T:Text7;6.67,53.66,0,3,4.59,k;"+ date.toString() + "\r\n\
      //     T:Text8;6.67,60.74,0,3,4.59,k;"+ time.toString() + "\r\n\
      //     A 1\r\n";                        
      //let hexString = "6D206D0D0A4A0D0A48203130300D0A53206C313B302C302C36382C37302C3130300D0A4F20520D0A542031302C31302C302C352C707432303B48455820544553540A422031302C32302C302C45414E2D31332C5343323B3430313233343531323334350D0A4720382C342C303B523A33302C392C302E332C302E330D0A4120310D0A";
      // console.log(printerCommandString);
      // let hexString = G4EYringToHex(printerCommandString);
      let rawHex = Buffer.from(commands, 'hex');
      client.write(rawHex);

      client.end();
  });

  
  client.on('end', function() {
      console.log('Requested an end to the TCP connection');
  });
  
  res.json([data, owner]);
});

router.post('*', async function(req, res, next) {
  delete req.body.name;
  req.col = "";
  req.val = "";
  for (const key in req.body){
    req.col += key + ", " ;
    req.val += req.body[key] + ", ";
  }
  req.val = req.val.substring(0, req.val.length - 2);
  req.col = req.col.substring(0, req.col.length - 2);
  next();
});

router.get('*/measure', async function(req, res, next) {
  let data = await db.queryMulti([`set @total_length_links = (SELECT value from constants where name = "total_length_links");`,
  `set @heater_length_links = (SELECT value from constants where name = "heater_length_links");`,
  `set @chain_link_length_meters = (SELECT value from constants where name = "chain_link_length_meters");`,
  `set @width_meters = (SELECT value from constants where name = "width_meters");`,
  `set @multiplier = (0.001*@width_meters*@chain_link_length_meters*3600);`,
  `    with thickData as (
    SELECT lol.chain as chain, AVG(lol.distance) as thickness , MIN(lol.time) as time
FROM (
    SELECT chain, distance, time 
    from data1 
    where distance is not null 
    AND chain is not null
) as lol
GROUP by chain
)

SELECT grainAnalysisView.*, 
TIMESTAMPDIFF(SECOND, grainAnalysisView.time_in, grainAnalysisView.time_out) as dryerTime, 
AVG(thick.thickness) as avg_thickness,

-- tons per hour calculation
IF(
  grainAnalysisView.dens_out is not null,
  (AVG(thick.thickness)*@multiplier*(0.001*(grainAnalysisView.dens_in))*@total_length_links/TIMESTAMPDIFF(SECOND, MIN(thick.time), MAX(thick.time))),
  NULL
  )
 AS tph_full,


--   (AVG(thick.thickness)*@multiplier*(0.001*(grainAnalysisView.dens_in+grainAnalysisView.dens_out)/2)*@heater_length_links/TIMESTAMPDIFF(SECOND, MIN(heatTime.time), MAX(heatTime.time))) AS tph_heat,
--   (AVG(thick.thickness)*@multiplier*(@total_length_links-@heater_length_links)/TIMESTAMPDIFF(SECOND, MIN(coolTime.time), MAX(coolTime.time))) AS tph_cool,

-- percent tons per hour calculations wile78
(AVG(thick.thickness)*@multiplier*(0.001*(grainAnalysisView.dens_in))*@total_length_links*
  (grainAnalysisView.moist_in-grainAnalysisView.moist_out)
/TIMESTAMPDIFF(SECOND, MIN(thick.time), MAX(thick.time))) 
AS pth_full,

-- percent tons per hour calculations wile200
(AVG(thick.thickness)*@multiplier*(0.001*(grainAnalysisView.dens_in))*@total_length_links*
    (grainAnalysisView.moist_in_200-grainAnalysisView.moist_out_200)
/TIMESTAMPDIFF(SECOND, MIN(thick.time), MAX(thick.time))) 
AS pth_full_200


--    (AVG(thick.thickness)*@multiplier*(0.001*(grainAnalysisView.dens_in+grainAnalysisView.dens_out)/2)*@total_length_links*(grainAnalysisView.moist_in-grainAnalysisView.moist_out)/TIMESTAMPDIFF(SECOND, MIN(heatTime.time), MAX(heatTime.time))) AS pth_heat
FROM grainAnalysisView
left join thickData as thick on thick.chain BETWEEN grainAnalysisView.chain_in AND (grainAnalysisView.chain_in + @total_length_links)
--    left join thickData as heatTime on heatTime.chain BETWEEN grainAnalysisView.chain_in AND (grainAnalysisView.chain_in + @heater_length_links)
-- left join thickData as coolTime on coolTime.chain BETWEEN grainAnalysisView.chain_in+@heater_length_links AND grainAnalysisView.chain_in + @total_length_links 
where id > ((SELECT MAX(id) from grainAnalysisView)-30)
group by grainAnalysisView.id;`]);
  let formatedData = [];
  data.forEach(item => {
    formatedData.push({
    id: item.id,
    grainType: item.grainType,
    owner: item.owner,
    mode: item.legacy_mode,
    storage: item.storage,
    avg_thickness: item.avg_thickness,
    tph_full: item.tph_full,
    tph_heat: item.tph_heat,
    tph_cool:	 item.tph_cool,
    pth_full:	 item.pth_full,
    pth_full_200:	 item.pth_full_200,
    pth_heat: item.pth_heat,
    chain_out_calc: item.chain_out_calc,
    speed_calc: item.speed_calc,
    // mode_name: item.mode_name,
    // mode_depth: item.mode_depth,
    // mode_temperature: item.mode_temperature,
    // mode_blowspeed: item.mode_blowspeed,
    // mode_basemoisture: item.mode_basemoisture,
    // mode_pts: item.mode_pts,
    first: {
      time: item.time_in,
      moist: item.moist_in,
      moist_200: item.moist_in_200,
      temp: item.temp_in,
      temp_200: item.temp_in_200,
      dens: item.dens_in,
      chain: item.chain_in
    },
    second: {
      time: item.time_out,
      moist: item.moist_out,
      moist_200: item.moist_out_200,
      temp: item.temp_out,
      temp_200: item.temp_out_200,
      dens: item.dens_out,
      chain: item.chain_out
    }
    });
  });
  res.json(formatedData);
});



router.get('*/types', async function(req, res, next) {
  let data = await db.query(`SELECT * FROM grainTypes ORDER BY name ASC;`);
  res.json(data);
});
router.get('*/owners', async function(req, res, next) {
  let data = await db.query(`SELECT * FROM owners ORDER BY name ASC;`);
  res.json(data);
});
router.get('*/modes', async function(req, res, next) {
  let data = await db.query(`SELECT * FROM legacyModesView ORDER BY name ASC;`);
  res.json(data);
});
router.get('*/measure/last', async function(req, res, next) {
  let data = await db.query(`SELECT grainType, owner, legacy_mode FROM grainAnalysis ORDER BY id DESC LIMIT 1;`);
  res.json(data);
});


router.post('*/modes', async function(req, res, next) {
  delete req.body.name;
  req.col = "";
  req.val = "";
  for (const key in req.body){
    req.col += key + ", " ;
    req.val += key === "modifier" ? `"${req.body[key]}", ` : req.body[key] + ", ";
  }
  req.val = req.val.substring(0, req.val.length - 2);
  req.col = req.col.substring(0, req.col.length - 2);

  let data;
  try {
    console.log(`INSERT INTO legacy_modes (${req.col}) VALUES (${req.val});`)
    data = await db.query(`INSERT INTO legacy_modes (${req.col}) VALUES (${req.val});`);
  } catch (e) {
    res.status(500).json({data});
    return;
  }
  // if(req.params.analysisId){
  //   data2 = await db.query(`UPDATE grainAnalysis SET measurement_out=${data.insertId} WHERE id=${req.params.analysisId};`);
  // }else{
  //   let q1 =`INSERT INTO grainAnalysis (owner, grainType, measurement_in, chain_out_calc, legacy_mode) VALUES (${owner + ', ' + type + ', ' + data.insertId + ', ' + (req.body.chain + 5) +  ', ' + mode});`;
  //   let q2 =`INSERT INTO grainAnalysis (owner, grainType, measurement_out, legacy_mode) VALUES (${owner + ', ' + type + ', ' + data.insertId + ', ' + mode});`;
  //   data2 = await db.query(output ? q2 : q1);
  // }

  res.json({data});
});

router.post('*/measure/:analysisId', async function(req, res, next) {
  let type = req.body.grainType;
  delete req.body.grainType;
  let output = req.body.output;
  delete req.body.output;
  let owner = req.body.owner;
  delete req.body.owner;
  let mode = req.body.mode;
  delete req.body.mode;
  let speed_calc = req.body.speed_calc;
  delete req.body.speed_calc;
  req.col = "";
  req.val = "";
  for (const key in req.body){
    req.col += key + ", " ;
    req.val += (key === "time" ? `"${req.body[key]}", ` : `${req.body[key]}, `);
  }
  req.val = req.val.substring(0, req.val.length - 2);
  req.col = req.col.substring(0, req.col.length - 2);
  
  let data = await db.query(`INSERT INTO grainMeasurements (${req.col}) VALUES (${req.val});`);
  let data2 ={};
  req.params.analysisId = parseInt(req.params.analysisId, 10);
  let lastStorage = (await db.query(`SELECT storage FROM data1 WHERE storage is not null order by id desc limit 1;`))[0].storage;
  console.log(lastStorage);
  if(req.params.analysisId){
    data2 = await db.query(`UPDATE grainAnalysis SET measurement_out=${data.insertId}, storage=${lastStorage} WHERE id=${req.params.analysisId};`);
  }else{
    console.log(speed_calc);
    console.log(`INSERT INTO grainAnalysis (owner, grainType, measurement_in, chain_out_calc, legacy_mode ${speed_calc ? ", speed_calc" : ""}) VALUES (${owner + ', ' + type + ', ' + data.insertId + ', ' + (req.body.chain + 134) +  ', ' + mode + (speed_calc ? `, ${speed_calc}` : "")});`);

    let q1 =`INSERT INTO grainAnalysis (owner, grainType, measurement_in, chain_out_calc, legacy_mode ${speed_calc ? ", speed_calc" : ""}) VALUES (${owner + ', ' + type + ', ' + data.insertId + ', ' + (req.body.chain + 134) +  ', ' + mode + (speed_calc ? `, ${speed_calc}` : "")});`;
    let q2 =`INSERT INTO grainAnalysis (owner, grainType, measurement_out, legacy_mode, storage) VALUES (${owner + ', ' + type + ', ' + data.insertId + ', ' + mode + ', ' + lastStorage});`;
    data2 = await db.query(output ? q2 : q1);
  }

  res.json({data, data2});
});

router.put('*/modes/:id', async function(req, res, next) {
    let data = await db.query(`UPDATE legacy_modes SET modifier=${typeof req.body.modifier  === 'object' || !req.body.modifier ? null : `"${req.body.modifier}"`}, pts=${req.body.pts} WHERE id=${req.params.id};`);
  res.json({data});
});


// router.post('measure', async function(req, res, next) {
//   console.log(req.body);
//   // res.send(JSON.stringify(req.body));
//   let col = "", val = "";
//   for (const key in req.body){
//     col += key + ", " ;
//     val += req.body[key] + ", ";
//   }
//   val = val.substring(0, val.length - 2);
//   col = col.substring(0, col.length - 2);
//   console.log(col);
//   console.log(val);

//   let data = await db.query(`INSERT INTO grainParams (${req.col}) VALUES (${req.val});`);
//   res.json(req.body);
// });

module.exports = router;
