var express = require('express');
var router = express.Router();
const db = require('../db/db.js');

router.post('/', async function(req, res, next) {
console.log(req.body);
    req.col = "";
    req.val = "";
    for (const key in req.body){
        req.col += key + ", " ;
        req.val += `'${req.body[key]}'` + ", ";
    }
    req.val = req.val.substring(0, req.val.length - 2);
    req.col = req.col.substring(0, req.col.length - 2);
    let data = await db.query(`INSERT INTO datalog (${req.col}) VALUES (${req.val});`);
    // consolsde.log(data.insertId);
    res.json(data);
  });
  
  
  module.exports = router;