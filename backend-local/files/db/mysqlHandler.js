const mysql = require('mysql2/promise')


function query(){
  let res;
  const connection = mysql.createConnection({
    host: 'db',
    user: 'root',
    password: 'test',
    database: 'kalte'
  })
  connection.connect();

  connection.query('SELECT * FROM test', (err, rows, fields) => {
    if (err) throw err
    res = rows;
  })

  connection.end()
  return res;
}



module.exports = {query};