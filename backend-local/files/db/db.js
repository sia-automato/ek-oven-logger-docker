const mysql = require('mysql2/promise');
const creds = {
  host: 'db',
  user: 'root',
  password: 'test',
  database: 'oven',
  multipleStatements: true
}

async function query(sql, params) {
  const connection = await mysql.createConnection(creds);
  const [results, ] = await connection.execute(sql, params);
  connection.end();
  return results;
}


async function queryMulti(sql, params) {
  const connection = await mysql.createConnection(creds);
  let results;
  for (let i = 0; i < sql.length; i++) {

    [results, ] = await connection.execute(sql[i], params);
    
  }
  
  connection.end();
  return results;
}

module.exports = {
  query,
  queryMulti
}